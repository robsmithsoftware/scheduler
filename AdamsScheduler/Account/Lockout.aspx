﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Lockout.aspx.cs" Inherits="AdamsScheduler.Account.Lockout" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup>
        <h1>Locked out.</h1>
        <h2 class="text-danger">This account has either been locked out or has not been enabled.
            If you recently registered, an administrator will need to approve your account first.
            Please contant an administrator or try again later.</h2>
    </hgroup>
</asp:Content>
