﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AdamsScheduler.Models;
using System.Text;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdamsScheduler.Scheduler.Report
{
    public partial class TechnicianAppointmentReport : System.Web.UI.Page
    {
        DateTime StartDate;
        DateTime EndDate;
        List<Appointment> Appointments;

        protected void Page_Load(object sender, EventArgs e)
        {
            UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            Boolean isManager = manager.IsInRole(Context.User.Identity.GetUserId(), "Manager");

            //if (!isManager) { Response.Redirect("~/Scheduler"); }

            StartDate = DateTime.Parse(Request.QueryString["StartDate"].Replace("_", "-"));
            EndDate = DateTime.Parse(Request.QueryString["EndDate"].Replace("_", "-"));

            titleDate.InnerHtml = StartDate.ToShortDateString() + " - " + EndDate.ToShortDateString();

            GetDailyScheduleData();
        }

        private void GetDailyScheduleData()
        {
            Appointments = Appointment.LoadBySchedDateRange(StartDate, EndDate).OrderBy(x => x.AppointmentDate).ToList();
            StringBuilder sb = new StringBuilder();

            foreach (Appointment app in Appointments.GroupBy(x => x.AppointmentDate).Select(y => y.First()).ToList())
            {
                DateTime date = app.AppointmentDate;
                sb.Append("<div><h3>" + date.ToShortDateString() + "</h3>");
                sb.Append("<table class='table table-striped'>");
                sb.Append("<tr><td><strong>Technician</strong></td><td><strong>Customer</strong></td><td><strong>Address</strong></td><td><strong>Amount</strong></td><td><strong>Notes</strong></td></tr>");
                foreach (Appointment appt in Appointments.Where(x => x.AppointmentDate == date).OrderBy(x => x.TechnicianName).ToList())
                {
                    sb.Append("<tr><td>" + appt.TechnicianName + "</td><td>" + appt.CustomerName + "</td><td>" + appt.Address + " " + appt.City + "</td><td>$" + appt.Amount.ToString() + "</td><td>" + appt.Note + "</td></tr>");
                }
                sb.Append("</table></div>");
            }

            AppointmentTable.Text = sb.ToString();
            
        }

        protected void AppointmentRepeater_DataBinding(object sender, EventArgs e)
        {

        }
    }
}