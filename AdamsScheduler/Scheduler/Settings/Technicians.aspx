﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Technicians.aspx.cs" Inherits="AdamsScheduler.Scheduler.Settings.Technicians" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="technicianList" runat="server">
        <div class="table-responsive">
            <h2>Manage Technicians</h2>
            <a href="Technicians?TechnicianId=0" class="btn btn-primary">New</a><br />
            <asp:Literal id="listContent" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <asp:Panel ID="technicianDetail" runat="server">
        <h2>Edit Technician, ID:<span id="TechIdNumber" runat="server"></span></h2>
        <div class="form-inline">
            <div class="form-group">
                <input id="TechFirstName" type="text" class="form-control col-sm-6" runat="server" />
            </div>
            <div class="form-group">
                <input id="TechLastName" type="text" class="form-control col-sm-6" runat="server" />
            </div>
        </div>
        <br />
        <div class="btn-group">
            <a class="btn btn-success" style="width:150px;" runat="server" onserverclick="Save">Save</a>
            <a class="btn btn-warning" style="width:150px;" href="Technicians">Cancel</a>
        </div>
        <a class="btn btn-danger" style="width:150px;" id="deactivateButton" runat="server" onserverclick="Deactivate">Deactivate</a>
    </asp:Panel>
</asp:Content>
