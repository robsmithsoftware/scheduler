﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AdamsScheduler.Models;

namespace AdamsScheduler.Scheduler.Report
{
    public partial class DailySchedule : System.Web.UI.Page
    {
        DateTime SchedDate;

        protected void Page_Load(object sender, EventArgs e)
        {
            SchedDate = DateTime.Parse(Request.QueryString["SchedDate"].Replace("_", "-"));
            GetDailyScheduleData();
        }

        private void GetDailyScheduleData()
        {
            List<Appointment> appointments = Appointment.LoadBySchedDate(SchedDate);
            List<Technician> technicians = Technician.LoadTechniciansInAppointmentList(appointments);

            

            TechnicianRepeater.DataSource = technicians;
            TechnicianRepeater.DataBind();
        }

        protected void TechnicianRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Technician tech = (Technician)e.Item.DataItem;

            Repeater AppointmentRepeater = (Repeater)e.Item.FindControl("AppointmentRepeater");
            AppointmentRepeater.DataSource = Appointment.LoadBySchedDateAndTech(SchedDate, tech.TechID);
            AppointmentRepeater.DataBind();
        }

        protected void AppointmentRepeater_DataBinding(object sender, EventArgs e)
        {

        }
    }
}