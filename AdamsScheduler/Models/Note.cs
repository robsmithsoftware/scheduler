﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace AdamsScheduler.Models
{
    public class Note
    {
        private int _Id;
        private string _Date;
        private string _Text;
        private string _UserId;
        private string _User;
        private DateTime _AddDate;
        private DateTime? _EditDate;

        public int Id { get { return this._Id; } set { this._Id = value; } }
        public string Date { get { return this._Date; } set { this._Date = value; } }
        public string Text { get { return this._Text; } set { this._Text = value; } }
        public string UserId { get { return this._UserId; } set { this._UserId = value; } }
        public string User { get { return this._User; } set { this._User = value; } }
        public DateTime AddDate { get { return this._AddDate; } set { this._AddDate = value; } }
        public DateTime? EditDate { get { return this._EditDate; } set { this._EditDate = value; } }

        public Note()
        {
            this.Id = -1;
            this.AddDate = DateTime.Now;
            this.EditDate = DateTime.Now;
        }

        public Note(string date)
        {
            this.Id = -1;
            this.Date = date;
            this.Text = "";
            this.User = "";
            this.AddDate = DateTime.Now;
            this.EditDate = null;
        }

        #region Data Access Methods
        public bool Save()
        {
            bool retVal = false;

            // Set the query that will load a Techician by TechID.
            StringBuilder sb = new StringBuilder();
            sb.Append("IF EXISTS (Select * From app.Note Where Id = @NoteId) Begin ");
            sb.Append("UPDATE app.Note Set Date = @Date, Text = @Text, UserId = @UserId, EditDate = @AddDate Where Id = @NoteId Select 0 End Else Begin ");
            sb.Append("INSERT INTO app.Note(Date, Text, UserId, AddDate) Values (@Date, @Text, @UserId, GetDate()) SELECT Cast(SCOPE_IDENTITY() As int) End");
            string query = sb.ToString();

            // We are saving the note, so the current user now has ownership.
            this.UserId = HttpContext.Current.User.Identity.GetUserId();

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@NoteId", this.Id);
                command.Parameters.AddWithValue("@Date", this.Date);
                command.Parameters.AddWithValue("@Text", this.Text);
                command.Parameters.AddWithValue("@UserId", this.UserId);
                command.Parameters.AddWithValue("@AddDate", this.AddDate);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    this.Id = (int)command.ExecuteScalar();
                    conn.Close();

                    retVal = true;
                }
                catch (Exception ex)
                {
                    retVal = false;
                }
            }

            return retVal;
        }

        public bool LoadById(int NoteId)
        {
            bool retVal = false;

            // Set the query that will load a Techician by TechID.
            string query = "SELECT n.Id, n.Date, n.Text, n.UserId, u.UserName, n.AddDate, n.EditDate FROM app.Note n Inner Join accounts.AspNetUsers u on u.Id = n.UserId WHERE n.Id = @NoteId;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@NoteId", NoteId);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    if (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        this.Id = int.Parse(reader[0].ToString());
                        this.Date = reader[1].ToString();
                        this.Text = reader[2].ToString();
                        this.UserId = reader[3].ToString();
                        this.User = reader[4].ToString();
                        this.AddDate = DateTime.Parse(reader[5].ToString());
                        this.EditDate = DateTime.Parse(reader[6].ToString());

                        // Set retVal to true so we know the query returned a row.
                        retVal = true;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return retVal;
        }

        public bool LoadByDate(string noteDate)
        {
            bool retVal = false;

            // Set the query that will load a Techician by TechID.
            string query = "SELECT n.Id, n.Date, n.Text, n.UserId, u.UserName, n.AddDate, n.EditDate FROM app.Note n Inner Join accounts.AspNetUsers u on u.Id = n.UserId WHERE n.Date = @NoteDate;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@NoteDate", noteDate);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    if (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        this.Id = int.Parse(reader[0].ToString());
                        this.Date = reader[1].ToString();
                        this.Text = reader[2].ToString();
                        this.UserId = reader[3].ToString();
                        this.User = reader[4].ToString();
                        this.AddDate = DateTime.Parse(reader[5].ToString());
                        this.EditDate = DateTime.Parse(reader[6].ToString());

                        // Set retVal to true so we know the query returned a row.
                        retVal = true;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return retVal;
        }
        #endregion
    }
}