﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace AdamsScheduler.Models
{
    public class Technician
    {
        private int _TechID;
        private string _FirstName;
        private string _LastName;
        private bool _Active;

        public int TechID { get { return _TechID; } set { _TechID = value; } }
        public string FirstName { get { return _FirstName; } set { _FirstName = value; } }
        public string LastName { get { return _LastName; } set { _LastName = value; } }
        public bool Active { get { return _Active; } set { _Active = value; } }

        public Technician()
        {
            this.TechID = 0;
            this.FirstName = "";
            this.LastName = "";
            this.Active = true;
        }

        public Technician(int TechID, string FirstName, string LastName, bool Active)
        {
            this.TechID = TechID;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Active = Active;
        }

        #region Data Access Methods
        
        // Load a technician from the DB. Returns true if successful, else false.
        public Boolean LoadByTechnicianID(int TechnicianID)
        {
            Boolean retVal = false;

            // Set the query that will load a Techician by TechID.
            string query = "SELECT TechnicianId, FirstName, LastName, Active FROM app.Technician WHERE TechnicianId = @TechnicianId;";
            
            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@TechnicianID", TechnicianID);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    
                    // Use if to get the first record.
                    if (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        this.TechID = int.Parse(reader[0].ToString());
                        this.FirstName = reader[1].ToString();
                        this.LastName = reader[2].ToString();
                        this.Active = bool.Parse(reader[3].ToString());

                        // Set retVal to true so we know the query returned a row.
                        retVal = true;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return retVal;
        }

        // Insert a technician into the DB. Returns this technicians TechID.
        public int SaveTechnician()
        {
            // Set the query that will load a Techician by TechID.
            StringBuilder sb = new StringBuilder();
            sb.Append("IF EXISTS (Select * From app.Technician Where TechnicianId = @TechId) Begin ");
            sb.Append("UPDATE app.Technician Set FirstName = @First, LastName = @Last, Active = @Active Where TechnicianId = @TechId Select 0 End Else Begin ");
            sb.Append("INSERT INTO app.Technician(FirstName, LastName, Active) Values (@First, @Last, @Active) SELECT Cast(SCOPE_IDENTITY() As int) End");
            string query = sb.ToString();

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@TechId", this.TechID);
                command.Parameters.AddWithValue("@First", this.FirstName);
                command.Parameters.AddWithValue("@Last", this.LastName);
                command.Parameters.AddWithValue("@Active", this.Active);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    this.TechID = (int)command.ExecuteScalar();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    this.TechID = 0;
                }
            }

            return this.TechID;
        }

        // Load a list of technicians from the DB which matches a list of appointments.
        public static List<Technician> LoadTechniciansInAppointmentList(List<Appointment> Appointments)
        {
            List<Technician> technicians = new List<Technician>();

            // Get a list of the TechId's referenced in this appointment list.
            List<int> techIDs = new List<int>();
            foreach (Appointment appointment in Appointments)
            {
                techIDs.Add(appointment.TechnicianID);
            }

            // Get a list of all technicians from the database. This is the fastest way to do this that I've found.
            List<Technician> allTechnicians = new List<Technician>();

            // Set the query that will load a Techician by TechID.
            string query = "SELECT TechnicianId, FirstName, LastName, Active FROM app.Technician ORDER BY FirstName, LastName;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                
                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    while (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        int TechID = int.Parse(reader[0].ToString());
                        string FirstName = reader[1].ToString();
                        string LastName = reader[2].ToString();
                        bool Active = bool.Parse(reader[3].ToString());

                        // Add this technician to the list.
                        allTechnicians.Add(new Technician(TechID, FirstName, LastName, Active));
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }

            // Now filter the list, removing techs that don't have an appointment today.
            foreach (Technician technician in allTechnicians)
            {
                if (techIDs.IndexOf(technician.TechID) != -1)
                {
                    technicians.Add(technician);
                }
            }

            return technicians;
        }

        // Load a list of all technicians from the DB.
        public static List<Technician> LoadAllTechnicians()
        {
            // Get a list of all technicians from the database. This is the fastest way to do this that I've found.
            List<Technician> allTechnicians = new List<Technician>();

            // Set the query that will load a Techician by TechID.
            string query = "SELECT TechnicianId, FirstName, LastName, Active FROM app.Technician ORDER BY Active Desc, FirstName, LastName;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    while (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        int TechID = int.Parse(reader[0].ToString());
                        string FirstName = reader[1].ToString();
                        string LastName = reader[2].ToString();
                        bool Active = bool.Parse(reader[3].ToString());

                        // Add this technician to the list.
                        allTechnicians.Add(new Technician(TechID, FirstName, LastName, Active));
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return allTechnicians;
        }

        #endregion
    }
}