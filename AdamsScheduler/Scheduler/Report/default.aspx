﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdamsScheduler.Scheduler.Report._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Reports</h1>
    <div class="panel col-sm-6">
        <ul id="ReportList">
            <li id="TechnicianAppointment">
                <a>Technician Appointment</a>
                <div class="reportParameters form-inline">
                    <div class="col-sm-5 form-group">
                        <input class="StartDate form-control form-control-sm" placeholder="Start Date" />
                    </div>
                    <div class="col-sm-5 form-group">
                        <input class="EndDate form-control form-control-sm" placeholder="End Date" />
                    </div>
                    <div class="col-sm-2 form-group">
                        <a id="RunTechnicianAppointment" class="btn btn-primary">Run</a>
                    </div>
                </div>
            </li>
            <li id="TechnicianRouteForecast">
                <a>Technician Route Forecast</a>
                <div class="reportParameters form-inline">
                    <div class="col-sm-3 form-group">
                        <input class="StartDate form-control form-control-sm" placeholder="Start Date" />
                    </div>
                    <div class="col-sm-3 form-group">
                        <input class="EndDate form-control form-control-sm" placeholder="End Date" />
                    </div>
                    <div class="col-sm-4 form-group">
                        <asp:DropDownList runat="server" class="form-control TechnicianList" id="TechnicianList"></asp:DropDownList>
                    </div>
                    <div class="col-sm-2 form-group">
                        <a id="RunTechnicianRouteForecast" class="btn btn-primary">Run</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">
        $("#TechnicianAppointment .reportParameters a").click(runTechnicianAppointmentReport);
        $("#TechnicianRouteForecast .reportParameters a").click(runTechnicianRouteForecast);

        $("#ReportList li a").click(function () {
            $(this).parent().parent().find("li").not($(this).parent()).find(".reportParameters").slideUp();
            $(this).parent().find(".reportParameters").slideToggle();
        });

        function runTechnicianAppointmentReport() {
            var StartDate = $("#TechnicianAppointment .StartDate").val();
            var EndDate = $("#TechnicianAppointment .EndDate").val();

            var url = "TechnicianAppointmentReport" + "?StartDate=" + StartDate + "&EndDate=" + EndDate;
            window.open(url, '_blank');
        }

        function runTechnicianRouteForecast() {
            var StartDate = $("#TechnicianRouteForecast .StartDate").val();
            var EndDate = $("#TechnicianRouteForecast .EndDate").val();
            var TechId = $("#TechnicianRouteForecast .TechnicianList").val();

            var url = "TechnicianRouteForecast" + "?StartDate=" + StartDate + "&EndDate=" + EndDate + "&TechId=" + TechId;
            window.open(url, '_blank');
        }
    </script>
</asp:Content>
