﻿const range = 11;
const startTime = 7;

function initTimeLines(data) {
    data.sort(function (obj1, obj2) {
        return obj1.TechID - obj2.TechID;
    });

    //if ($("div#timeline").width() % (range * 20) != 0) {
    //    $("div#timeline").width($("div#timeline").width() - ($("div#timeline").width() % (range * 20)) + 2);
    //    $("div#details").width($("div#timeline").width());
    //}

    var techIDs = getFields(data, "TechnicianID");
    var TechAppointments
    techIDs.forEach(function (element, index, array) {
        var TechAppointments = data.filter(function (value) { return value[0]["TechnicianID"] == element });
        TechAppointments[0].forEach(function (elementTA, indexTA, arrayTA) {
            $("[TechID='" + elementTA.TechnicianID + "']").find("div#timeline").prepend(
                "<div class='appointment' schedid=" + elementTA.AppointmentID + "></div>"
                );
            $("[schedid='" + elementTA.AppointmentID + "'").attr('start', elementTA.StartTime);
            $("[schedid='" + elementTA.AppointmentID + "'").attr('end', elementTA.EndTime);
            $("[schedid='" + elementTA.AppointmentID + "'").attr('travel', elementTA.TravelTime);
            $("[schedid='" + elementTA.AppointmentID + "'").attr('locked', elementTA.Locked);
            $("[schedid='" + elementTA.AppointmentID + "'").attr('address', elementTA.Address + " " + elementTA.City + " " + elementTA.State + " " + elementTA.Zip );
            $("[schedid='" + elementTA.AppointmentID + "'").css('background-color', elementTA.Color);
            $("[schedid='" + elementTA.AppointmentID + "'").attr('customer', elementTA.CustomerName);
        });
    });

    $("div#timeline").find("[schedid]").not("[locked='true']").draggable({
        axis: "x",
        containment: "parent",
        drag: shiftAppt
    });

    drawAppts();

    // Build "dynamic" rulers by adding items
    $(".ruler[data-items]").each(function () {
        var ruler = $(this).empty(),
            len = Number(ruler.attr("data-items")) || 0,
            item = $(document.createElement("li")),
            i;
        for (i = startTime; i < len + startTime; i++) {
            if ((i - startTime + 1) % 4 == 0) {
                ruler.append(item.clone().text(function(){
                    return (((i - startTime + 1) / 4) + startTime) % 12 || 12;
                }));
            } else {
                ruler.append(item.clone().text(''));
            }
        }
    });

    var spacing = $("div#timeline").first().width() / (range * 4);
    //var spacing = 0;
    $(".ruler").
          find("li").
            css("padding-left", spacing);
}

function drawAppts() {
    $("div#timeline").each(function () {
        drawTechAppts($(this).closest("[techid]").attr("techid"));
    });
}

function drawTechAppts(techId) {
    $("div[techid=" + techId.toString() + "]").find("div#timeline").find("[schedid]").each(function () {
        $(this).html("");
        var totalPx = $(this).parent().parent().width();
        var totalSec = range * 60;

        var contWidth = $(this).parent().width();
        var width = parseInt($(this).attr("end")) - parseInt($(this).attr("start")) + parseInt($(this).attr("travel"));
        var startSec = parseInt($(this).attr("start")) - parseInt($(this).attr("travel"));
        var startPx = (startSec - (startTime * 60)) * (totalPx / totalSec);

        $(this).css("left", startPx);
        $(this).css("width", Math.ceil(Math.round(((contWidth / (range * 60)) * width) / (contWidth / (range * 20))) * (contWidth / (range * 20))));
        $(this).prepend("<div class='apptOnly'><div class='CustomerName'>" + $(this).attr("Customer") + "</div><div class='apptTime'></div><a id='apptDetails' class='btn btn-info btn-xs'>Details</a></div>");
        $(this).find(".apptOnly").css("width", $(this).width());
        if ($(this).attr("locked") == "true")
            $(this).append(
                '<i id="lockedAppt" class="fa fa-lock fa-2" aria-hidden="true"></i>'
            );
        $(this).prepend("<div style='width:" + ((contWidth / (range * 60)) * $(this).attr("travel")).toString() + "px;' class='travel'></div>");
        $(this).find(".apptOnly").css("padding-left", $(this).find(".travel").width());

        fillTime($(this));
    });
}

function getFields(input, field) {
    var output = [];
    for (var i = 0; i < input.length ; ++i)
        if (!output.includes(input[i][0][field])) {
            output.push(input[i][0][field]);
        }
    return output;
}

function shiftAppt(event, ui) {
    var target = ui.helper[0];

    var originalLeft = $(target).position().left;
    var currentLeft = ui.position.left;

    var otherDivs = $(target).parent().find(".appointment").not("[locked='true']");

    var originalRight = $(target).position().left + $(target).width();
    var currentRight = ui.position.left + $(target).width();

    otherDivs.each(function () {
        if ($(this).attr("schedid") != $(target).attr("schedid") && $(this).position().left > currentLeft && $(this).position().left < originalLeft) {
            $(this).animate({
                left: "+=" + $(target).width().toString()
            }, 200, function () { fillTime($(this)); });
        }
    });

    otherDivs.each(function () {
        if ($(this).attr("schedid") != $(target).attr("schedid") && $(this).position().left + $(this).width() < currentRight && $(this).position().left + $(this).width() > originalRight) {
            $(this).animate({
                left: "-=" + $(target).width().toString()
            }, 200, function () { fillTime($(this)); });
        }
    });
    fillTime(target);
    showEditedButtons();
}

function fillTime(appointmentDiv) {
    $(appointmentDiv).find("div.apptTime").html(function () {
        var totalPx = $(appointmentDiv).parent().parent().width();

        var startPx = $(appointmentDiv).position().left + $(appointmentDiv).find(".travel").width();
        var endPx = $(appointmentDiv).position().left + $(appointmentDiv).width();

        var totalSec = range * 60;
        var startSec = ((startPx / totalPx) * totalSec) + (startTime * 60);
        var endSec = startSec + parseInt($(this).parent().parent().attr("end")) - parseInt($(this).parent().parent().attr("start"));

        return "<span id='startTime'>" + startSec.toString().toTime() + "</span> - <span id='endTime'>" + endSec.toString().toTime() + "</span>";
    });
}

String.prototype.toTime = function () {
    var sec_num = Math.round(parseFloat(this)); // don't forget the second param
    var hours = Math.floor((Math.ceil(sec_num * 100) / 100) / 60);
    var minutes = Math.round(sec_num - (hours * 60));
    if (minutes == 60) { minutes = 0; hours++;}
    if (minutes < 10) { minutes = "0" + minutes; }
    var time = (hours % 12 || 12) + ':' + minutes;
    return time;
}

function showEditedButtons() {
    if ($(".apptEdited").css('opacity') == '0') {
        $(".apptEdited").show();
        $(".apptEdited").animate({ opacity: 1 });
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function swapElement(array, indexA, indexB) {
    var tmp = array[indexA];
    array[indexA] = array[indexB];
    array[indexB] = tmp;
}

function addNotesToPanel(date, panel) {
    $.ajax({
        url: "../API/Notes/" + date,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (res) {
            $(panel).find(".panel-body").html("<textarea class='form-control' data-noteId='" + res.Id.toString() + "' data-noteDate='" + res.Date.toString() + "'>" + res.Text + "</textarea>");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $(panel).find(".panel-body").append("Error retrieving notes.");
        }
    });
}

function saveNotes(note) {
    $.ajax({
        url: "../API/Notes",
        type: 'POST',
        dataType: 'json', // added data type
        data: note,
        success: function (res) {
            alert("Success");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(throwError);
        }
    });
}

function Note(id, date, text) {
    this.Id = id;
    this.Date = date;
    this.Text = text;
}