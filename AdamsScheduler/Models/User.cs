﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace AdamsScheduler.Models
{
    public class User
    {
        private string _Id;
        private string _UserName;
        private bool _Enabled;
        
        public List<string> Roles;
        public string Id { get { return _Id; } set { _Id = value; } }
        public string UserName { get { return _UserName; } set { _UserName = value; } }
        public bool Enabled { get { return _Enabled; } set { _Enabled = value; } }
        public DateTime lockoutDate { get { return (Enabled ? new DateTime(1950, 1, 1) : new DateTime(9999, 12, 30)); } }

        public User()
        {
            this.Roles = new List<string>();
        }

        #region Data Access Methods
        public bool LoadByUserId(string userId)
        {
            Boolean retVal = false;

            // Set the query that will load a Techician by TechID.
            string query = "SELECT Id, UserName, IsNull(LockoutEndDateUtc, '1/1/1900') FROM accounts.AspNetUsers WHERE Id = @UserId;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@UserId", userId);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    if (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        this.Id = reader[0].ToString();
                        this.UserName = reader[1].ToString();
                        this.Enabled = DateTime.Parse(reader[2].ToString()) <= DateTime.Today;

                        // Set retVal to true so we know the query returned a row.
                        retVal = true;
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return retVal;
        }

        public static List<User> LoadAllUsers()
        {
            // Get a list of all technicians from the database. This is the fastest way to do this that I've found.
            List<User> allUsers = new List<User>();

            // Set the query that will load a Techician by TechID.
            string query = "SELECT Id, UserName, IsNull(LockoutEndDateUtc, '1/1/1900') FROM accounts.AspNetUsers ORDER BY UserName;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    while (reader.Read())
                    {
                        User user = new User();

                        // Set this objects properties to the properties of the result.
                        user.Id = reader[0].ToString();
                        user.UserName = reader[1].ToString();
                        user.Enabled = DateTime.Parse(reader[2].ToString()) <= DateTime.Today;

                        // Add this technician to the list.
                        allUsers.Add(user);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return allUsers;
        }

        public bool Save()
        {
            bool retVal = false;

            // Set the query that will load a Techician by TechID.
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE accounts.AspNetUsers Set ");
            sb.Append("UserName = @UserName, ");
            sb.Append("LockoutEndDateUtc = @LockDate ");
            sb.Append("Where Id = @UserId;");
            string query = sb.ToString();

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@UserId", this.Id);
                command.Parameters.AddWithValue("@Username", this.UserName);
                command.Parameters.AddWithValue("@LockDate", this.lockoutDate);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    retVal = command.ExecuteNonQuery() > 0;
                    conn.Close();
                }
                catch (Exception ex)
                {
                    retVal = false;
                }
            }

            return retVal;
        }

        public bool SaveRoles()
        {
            bool retVal = false;

            // Set the query that will load a Techician by TechID.
            string delete = "DELETE FROM accounts.AspNetUserRoles Where UserId = @UserId;";
            string query = "INSERT INTO accounts.AspNetUserRoles(UserId, RoleId) Select @UserId, Id From accounts.AspNetRoles Where Name = @Role;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();

                    SqlCommand deleteCommand = new SqlCommand(delete, conn);
                    deleteCommand.Parameters.AddWithValue("@UserId", this.Id);
                    deleteCommand.ExecuteNonQuery();

                    foreach (string role in this.Roles)
                    {
                        // Create the command.
                        SqlCommand queryCommand = new SqlCommand(query, conn);
                        queryCommand.Parameters.AddWithValue("@UserId", this.Id);
                        queryCommand.Parameters.AddWithValue("@Role", role);

                        queryCommand.ExecuteNonQuery();
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    
                }
            }

            return retVal;
        }
        #endregion
    }
}