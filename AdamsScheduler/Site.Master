﻿<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Site.master.cs" Inherits="AdamsScheduler.SiteMaster" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Adams Exterminators Scheduler</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="~/css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="~/css/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print" />
    <link href="~/css/scheduler.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <form runat="server">
        <asp:ScriptManager runat="server" EnablePageMethods="true">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/fullcalendar.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/TimeLine.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/routeOptimizer.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/bootstrap-timepicker.min.js")%>"></script>
        
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" runat="server" href="~/Scheduler">Adams Scheduler</a>
                </div>
                <div class="navbar-collapse collapse">
                    <asp:LoginView runat="server" ViewStateMode="Disabled">
                        <AnonymousTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Register">Register</a></li>
                                <li><a runat="server" href="~/Default">Log in</a></li>
                            </ul>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Manage" title="Manage your account">Hello, <%: Context.User.Identity.GetUserName()  %> !</a></li>
                                <li><a runat="server" href="~/Scheduler/Report/">Reports</a></li>
                                <li><a runat="server" href="~/Scheduler/Settings/">Settings</a></li>
                                <li>
                                    <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/Default" OnLoggingOut="Unnamed_LoggingOut" />
                                </li>
                            </ul>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
            </div>
        </div>
        <div class="container body-content">
            <asp:ContentPlaceHolder ID="MainContent" runat="server">
            </asp:ContentPlaceHolder>
            <hr />
            <footer>
                <p>&copy; <%: DateTime.Now.Year %> - Rob Smith Software</p>
            </footer>
        </div>
    </form>
    <script>
        $('.timepicker').timepicker();
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK5QuB3CXQYwkRtv9Yt-faUPt_s1y7c9I&callback=initMap" type="text/javascript"></script>
</body>
</html>
