﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AdamsScheduler.Models;

namespace AdamsScheduler.Scheduler.Report
{
    public partial class TechnicianDirections : System.Web.UI.Page
    {
        Technician technician;
        List<Appointment> appointments = new List<Appointment>();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Inititialize some stuff.
            int TechId = 0;
            DateTime SchedDate;
            technician = new Technician();

            // Try to get stuff passed in through the query string.
            int.TryParse(Request.QueryString["TechId"].ToString(), out TechId);
            DateTime.TryParse(Request.QueryString["SchedDate"].ToString().Replace('_', '-'), out SchedDate);

            technician.LoadByTechnicianID(TechId);
            
            foreach(Appointment appt in Appointment.LoadBySchedDate(SchedDate))
            {
                if(appt.TechnicianID == TechId)
                {
                    appointments.Add(appt);
                }
            }

            techName.InnerHtml = technician.FirstName + ' ' + technician.LastName;
            schedDate.InnerHtml = SchedDate.ToLongDateString();
        }
    }
}