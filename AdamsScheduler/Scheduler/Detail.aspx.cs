﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AdamsScheduler.Models;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace AdamsScheduler.Scheduler
{
    public partial class Detail : System.Web.UI.Page
    {

        private DateTime SchedDate;
        private List<Appointment> Appointments;
        private List<Technician> Technicians;

        protected void Page_Load(object sender, EventArgs e)
        {
            SchedDate = DateTime.MinValue;
            if (Request.QueryString["SchedDate"] != null)
            {
                DateTime.TryParse(Request.QueryString["SchedDate"].ToString().Replace('_', '-'), out SchedDate);
            }
            
            if (!isValidDate(SchedDate))
            {
                RejectDate();
            }
            else
            {
                AcceptDate(SchedDate);
            }
        }

        protected void LoadAppointments(Object sender, RepeaterItemEventArgs e)
        {

            if (Appointments.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                    "PushAppointmentsToJavaScript",
                    "data.push(" + new JavaScriptSerializer().Serialize(Appointments) + ");",
                    true);
            }
        }

        [WebMethod]
        public static void UpdateTimes(string StartTime, string EndTime, int SchedID)
        {
            Appointment app = new Appointment();
            app.LoadByApptID(SchedID);

            app.StartTime = TimeSpan.Parse(StartTime).TotalMinutes;
            app.EndTime = TimeSpan.Parse(EndTime).TotalMinutes;

            if (app.StartTime < TimeSpan.Parse("7:00").TotalMinutes)
                app.StartTime += TimeSpan.Parse("12:00").TotalMinutes;

            if (app.EndTime < TimeSpan.Parse("7:00").TotalMinutes)
                app.EndTime += TimeSpan.Parse("12:00").TotalMinutes;

            app.UpdateAppointment();
        }

        
        #region Helper Methods
        private bool isValidDate(DateTime date)
        {
            return !(date == DateTime.MinValue);
        }

        private void AcceptDate(DateTime SchedDate)
        {
            // Set the display date and add the sched date to the query string for AddAppointment.aspx.
            DisplayDate.InnerHtml = SchedDate.ToLongDateString();
            ApptAdd.HRef = ResolveUrl("~/Scheduler/AddAppointment") + "?SchedDate=" + Request.QueryString["SchedDate"];
            
            // Load a list of appointments for today and their technicians.
            Appointments = Appointment.LoadBySchedDate(SchedDate);
            Technicians = Technician.LoadTechniciansInAppointmentList(Appointments);

            // Do some stuff if we have an appointment.
            noResults.Visible = (Appointments.Count == 0);
            routeMap.Visible = !noResults.Visible;

            // Populate the map choices.
            List<ListItem> items = new List<ListItem>();

            foreach (Technician tech in Technicians)
            {
                items.Add(new ListItem(tech.FirstName + " " + tech.LastName, tech.TechID.ToString()));
            }

            mapChoices.DataTextField = "Text";
            mapChoices.DataValueField = "Value";
            mapChoices.DataSource = items;
            mapChoices.DataBind();


            TechList.DataSource = Technicians;
            TechList.DataBind();

            InitTimelines();
        }

        private void RejectDate()
        {
            DisplayDate.InnerHtml = "Invalid Date!";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                "err_msg",
                "alert('Error: Not a valid date!'); window.location='" + Page.ResolveUrl("~/Scheduler") + "';",
                true);
        }

        private void InitTimelines()
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                "init",
                "initTimeLines(data);",
                true);
        }
        #endregion

        protected void prevDate_ServerClick(object sender, EventArgs e)
        {
            // Get the current date, decrement it, and redirect to that page.
            DateTime.TryParse(Request.QueryString["SchedDate"].ToString().Replace('_', '-'), out SchedDate);
            SchedDate = SchedDate.AddDays(-1);
            string url = ResolveUrl("~/Scheduler/Detail") + "?SchedDate=" + SchedDate.ToShortDateString().ToString().Replace('/', '_');
            Response.Redirect(url);
        }

        protected void nextDate_ServerClick(object sender, EventArgs e)
        {
            // Get the current date, increment it, and redirect to that page.
            DateTime.TryParse(Request.QueryString["SchedDate"].ToString().Replace('_', '-'), out SchedDate);
            SchedDate = SchedDate.AddDays(1);
            string url = ResolveUrl("~/Scheduler/Detail") + "?SchedDate=" + SchedDate.ToShortDateString().ToString().Replace('/', '_');
            Response.Redirect(url);
        }
    }
}