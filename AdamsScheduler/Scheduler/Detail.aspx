﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="AdamsScheduler.Scheduler.Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="row">
        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li><a id="ApptAdd" runat="server">Add Appointment</a></li>
                <li><a href="<%=ResolveUrl("~/Scheduler") %>">Return to Calendar</a>
                <li><a id="DailyScheduleReport" href="#">Daily Schedule Report</a>
                <li><a id="optimizeDay" href="#">Optimize All Routes</a></li>
            </ul>
            <ul class="nav navbar-nav apptEdited">
                <li><a href="#" id="saveEdited">Save Changes</a></li>
                <li><asp:LinkButton runat="server" id="cancelEdited" class="">Cancel</asp:LinkButton></li>
            </ul>
            <div class="nav navbar-nav text-center btn-group pull-right pad-5">
                <button runat="server" id="prevDate" class="btn btn-primary pull-left" onserverclick="prevDate_ServerClick">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </button>
                <div class="btn btn-default pull-left" id="dateHolder">
                    <strong id="DisplayDate" runat="server">April 28, 2016</strong>
                </div>
                <button runat="server" id="nextDate" class="btn btn-primary pull-left" onserverclick="nextDate_ServerClick">
                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </button>
            </div>
        </nav>
    </div>
    <asp:Repeater id="TechList" runat="server" OnItemDataBound="LoadAppointments">
        <ItemTemplate>
            <div class="row" TechID="<%#Eval("TechID") %>">
                <div class="col-sm-12">
                    <div class="pull-left"><h3><%#Eval("FirstName") %> <%#Eval("LastName") %></h3></div>
                    <div class="pull-left btn-group tech-btns">
                        <a href="#" class="btn btn-primary tech-btn optimizeRoute">Optimize Route</a>
                        <a href="#" class="btn btn-primary tech-btn" id="directionsDisplay">Print Directions</a>
                    </div>
                    <div id="totalTimes">
                        <div class="text-right"><h3>Total Travel Time: <span id="travelTotal"></span></h3></div>
                        <div class="text-right"><h3>Total Production Time: <span id="productionTotal"></span></h3></div><div class="clearfix"></div>
                    </div>
                    <div id="timeline">
                        <ul class="ruler" data-items="44"></ul>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="row">
        <div runat="server" id="noResults" class="alert alert-danger text-center col-sm-6 col-sm-offset-3">
            <h3>No Appointments Scheduled!</h3>
        </div>
    </div>
    <asp:Panel id="routeMap" runat="server">
        <div class="row">
            <div class="col-sm-4">
                <asp:DropDownList runat="server" id="mapChoices" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="map"></div>                    
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        var data = [];

        // Handle detail button clicks.
        $(document).on('click', '#apptDetails', function () {
            window.location = "<%=ResolveUrl("~/Scheduler/AddAppointment") %>?SchedDate=" + getParameterByName("SchedDate") + "&ApptId=" + $(this).parent().parent().attr("SchedId");
        });

        $(document).on('click', '#DailyScheduleReport', function () {
            window.open("<%=ResolveUrl("~/Scheduler/Report/DailySchedule") %>?SchedDate=" + getParameterByName("SchedDate"));
        });

        $("#map").width($("#timeline").width());

        if ($("#timeline").length > 0) {
            $("#optimizeDay").css("display","block");
        }

        $('[id$="directionsDisplay"]').on('click', function () {
            var techId = $(this).closest('[TechID]').attr('TechID');
            var w = window.open('Report/TechnicianDirections?TechId=' + techId.toString() + '&Scheddate=' + getParameterByName("SchedDate"));
        });
    </script>

</asp:Content>
