﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddAppointment.aspx.cs" Inherits="AdamsScheduler.Scheduler.AddAppointment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-sm-12">
            <div id="saveSuccess" class="alert alert-success" runat="server" visible="false">
                Save Successful!
            </div>
            <div id="saveFailure" class="alert alert-danger" runat="server" visible="false">
                <h3>Save Failed!</h3>
                <span id="failureMessage" runat="server"></span>                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2>Adding Appointment for <span id="DisplayDate" runat="server">April 28, 2016</span>:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <fieldset class="form-group">
                <label for="TechnicianList">Technician</label>
                <asp:DropDownList runat="server" class="form-control" id="TechnicianList"></asp:DropDownList>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <fieldset class="form-group">
                <label for="CustomerName">Customer Name</label>
                <input runat="server" type="text" class="form-control" id="CustomerName" />
            </fieldset>
        </div>
        <div class="col-sm-4">
            <fieldset class="form-group">
                <label for="Salesman">Salesman</label>
                <input runat="server" type="text" class="form-control" id="Salesman" />
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <fieldset class="form-group">
                <label for="StartTime">Start Time</label>
                <input runat="server" type="text" class="form-control timepicker" id="StartTime" />
            </fieldset>
        </div>
        <div class="col-sm-3">
            <fieldset class="form-group">
                <label for="EndTime">End Time</label>
                <input runat="server" type="text" class="form-control timepicker" id="EndTime" />
            </fieldset>
        </div>
        <div class="col-sm-3">
            <fieldset class="form-group">
                <label for="Amount">Amount</label>
                <input runat="server" type="number" class="form-control" id="Amount" />
            </fieldset>
        </div>
        <div class="col-sm-3">
            <fieldset class="form-group">
                <label for="Locked">Locked</label>
                <input runat="server" type="checkbox" class="form-control" id="Locked" />
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <fieldset class="form-group">
                <label for="Address">Street Address</label>
                <input runat="server" type="text" class="form-control" id="Address" />
            </fieldset>
        </div>
        <div class="col-sm-3">
            <fieldset class="form-group">
                <label for="City">City</label>
                <input runat="server" type="text" class="form-control" id="City" />
            </fieldset>
        </div>
        <div class="col-sm-2">
            <fieldset class="form-group">
                <label for="State">State</label>
                <asp:DropDownList runat="server" class="form-control" id="State">
                    <asp:ListItem Value="AL">AL</asp:ListItem>
                    <asp:ListItem Value="AK">AK</asp:ListItem>
                    <asp:ListItem Value="AZ">AZ</asp:ListItem>
                    <asp:ListItem Value="AR">AR</asp:ListItem>
                    <asp:ListItem Value="CA">CA</asp:ListItem>
                    <asp:ListItem Value="CO">CO</asp:ListItem>
                    <asp:ListItem Value="CT">CT</asp:ListItem>
                    <asp:ListItem Value="DE">DE</asp:ListItem>
                    <asp:ListItem Value="FL">FL</asp:ListItem>
                    <asp:ListItem Value="GA" Selected="True">GA</asp:ListItem>
                    <asp:ListItem Value="HI">HI</asp:ListItem>
                    <asp:ListItem Value="ID">ID</asp:ListItem>
                    <asp:ListItem Value="IL">IL</asp:ListItem>
                    <asp:ListItem Value="IN">IN</asp:ListItem>
                    <asp:ListItem Value="IA">IA</asp:ListItem>
                    <asp:ListItem Value="KS">KS</asp:ListItem>
                    <asp:ListItem Value="KY">KY</asp:ListItem>
                    <asp:ListItem Value="LA">LA</asp:ListItem>
                    <asp:ListItem Value="ME">ME</asp:ListItem>
                    <asp:ListItem Value="MD">MD</asp:ListItem>
                    <asp:ListItem Value="MA">MA</asp:ListItem>
                    <asp:ListItem Value="MI">MI</asp:ListItem>
                    <asp:ListItem Value="MN">MN</asp:ListItem>
                    <asp:ListItem Value="MS">MS</asp:ListItem>
                    <asp:ListItem Value="MO">MO</asp:ListItem>
                    <asp:ListItem Value="MT">MT</asp:ListItem>
                    <asp:ListItem Value="NE">NE</asp:ListItem>
                    <asp:ListItem Value="NV">NV</asp:ListItem>
                    <asp:ListItem Value="NH">NH</asp:ListItem>
                    <asp:ListItem Value="NJ">NJ</asp:ListItem>
                    <asp:ListItem Value="NM">NM</asp:ListItem>
                    <asp:ListItem Value="NY">NY</asp:ListItem>
                    <asp:ListItem Value="ND">ND</asp:ListItem>
                    <asp:ListItem Value="OH">OH</asp:ListItem>
                    <asp:ListItem Value="OK">OK</asp:ListItem>
                    <asp:ListItem Value="OR">OR</asp:ListItem>
                    <asp:ListItem Value="PA">PA</asp:ListItem>
                    <asp:ListItem Value="RI">RI</asp:ListItem>
                    <asp:ListItem Value="SC">SC</asp:ListItem>
                    <asp:ListItem Value="SD">SD</asp:ListItem>
                    <asp:ListItem Value="TN">TN</asp:ListItem>
                    <asp:ListItem Value="TX">TX</asp:ListItem>
                    <asp:ListItem Value="UT">UT</asp:ListItem>
                    <asp:ListItem Value="VT">VT</asp:ListItem>
                    <asp:ListItem Value="VA">VA</asp:ListItem>
                    <asp:ListItem Value="WA">WA</asp:ListItem>
                    <asp:ListItem Value="WV">WV</asp:ListItem>
                    <asp:ListItem Value="WI">WI</asp:ListItem>
                    <asp:ListItem Value="WY">WY</asp:ListItem>
                </asp:DropDownList>
            </fieldset>
        </div>
        <div class="col-sm-3">
            <fieldset class="form-group">
                <label for="Zip">Zip</label>
                <input runat="server" type="text" class="form-control" id="Zip" />
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <fieldset class="form-group">
                <label for="Notes">Notes</label>
                <textarea id="Notes" class="form-control" runat="server" style="width:100%; height:150px;"></textarea>
            </fieldset>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group">
                <asp:LinkButton id="SaveAppt" runat="server" class="btn btn-success" Width="150px" onClick="SaveAppt_Click">Save Appointment</asp:LinkButton>
                <asp:LinkButton id="EditAppt" runat="server" class="btn btn-primary" Width="150px" OnClick="EditAppt_Click">Edit Appointment</asp:LinkButton>
                <asp:LinkButton id="DeleteAppt" runat="server" class="btn btn-danger" Width="150px" OnClick="DeleteAppt_Click">Delete Appointment</asp:LinkButton>
                <asp:LinkButton id="CancelAppt" runat="server" class="btn btn-warning" Width="150px" OnClick="CancelAppt_Click">Cancel</asp:LinkButton>
                <asp:LinkButton id="ExitAppt" runat="server" class="btn btn-info" Width="150px" OnClick="ExitAppt_Click">Exit</asp:LinkButton>
                <button id="ChangeDate" type="button" class="btn btn-primary" data-toggle="modal" data-target="#dateChangeModal" runat="server">Change Appointment Date</button>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="dateChangeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Appointment Date</h4>
                </div>
                <div class="modal-body">
                    <strong>Move appointment to:</strong>
                    <input type="text" id="datepicker" class="form-control" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="saveNewDate" type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $("#datepicker").datepicker();
        });

        $("#saveNewDate").click(function () {
            var dateParts = [];
            dateParts = $("#datepicker").val().split("/")
            var tempYear = dateParts[2];
            dateParts[2] = dateParts[1];
            dateParts[1] = dateParts[0];
            dateParts[0] = tempYear;

            var newDate = dateParts.join("_");

            window.location = "<%=ResolveUrl("~/Scheduler/AddAppointment") %>?SchedDate=" + newDate + "&ApptId=" + getParameterByName("ApptId");
        });
    </script>
</asp:Content>
