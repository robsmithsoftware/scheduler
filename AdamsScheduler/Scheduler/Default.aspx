﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdamsScheduler._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-sm-12"><h1 id="dateHeading">Today is </h1></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div id="note-panel" class="panel panel-primary">
                <div class="panel-heading">
                    <strong>Notes for <span class="note-panel-date"></span></strong>
                </div>
                <div class="panel-body">
                    
                </div>
                <div class="panel-footer">
                    <a class="btn btn-success" id="note-Save">Save</a>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div id="calendar"></div>
        </div>
    </div>

    
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.dateFormat-1.0.js")%>"></script>
    <script type="text/javascript">
        $('#dateHeading').append($.format.date(new Date(), "MMMM dd, yyyy"));
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                // Calendar options go here.
                header: {left: 'today',center: 'title',right: 'prev,next',},
                height: 500,
                dayClick: function (date, jsEvent, view) {
                    if (!$(jsEvent.target).hasClass('fc-note') && !$(jsEvent.target).hasClass('fa-sticky-note')) {
                        window.location = 'Detail?SchedDate=' + date.format().replaceAll('-', '_');
                    }
                }
            })

            // Prepend the notes icon to the days.
            loadNotes();

            // Reload the buttons every time the user goes to another month.
            $('.fc-button').click(function () {
                loadNotes();
            });

            $('#note-Save').click(function () {
                var Id = $(this).parent().parent().find('textarea').attr('data-noteId');
                var Date = $(this).parent().parent().find('textarea').attr('data-noteDate');
                var Text = $(this).parent().parent().find('textarea').val();

                var note = new Note(Id, Date,Text);

                saveNotes(note);
            });
        });

        function loadNotes() {
            $('.fc-day-number').prepend('<a class="fc-note btn btn-default btn-sm pull-left"><i class="fa fa-sticky-note fa-2" aria-hidden="true"></i></a>');
            $('.fc-note').click(function (event) {
                var date = $(this).parent().attr("data-date");
                $('#note-panel').slideUp("fast", function () {
                    addNotesToPanel(date, $(this));
                    $(this).find(".panel-body").html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>');
                    var dateArray = date.split('-');
                    swapElement(dateArray, 0, 1);
                    swapElement(dateArray, 1, 2);
                    $(this).find(".panel-heading .note-panel-date").html(dateArray.join('/'));
                    //$(this).
                    $(this).slideDown("fast");
                });
            });
        }
    </script>

</asp:Content>
