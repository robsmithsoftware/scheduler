﻿using AdamsScheduler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdamsScheduler.Scheduler.Report
{
    public partial class TechnicianRouteForecast : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Technician tech = new Technician();
            tech.LoadByTechnicianID(int.Parse(Request.QueryString["TechId"]));

            titleTech.InnerHtml = tech.FirstName + " " + tech.LastName;
        }

        [WebMethod]
        public static string GetAppointments(string Start, string End, int TechId)
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            DateTime startDate = DateTime.Parse(Start);
            DateTime endDate = DateTime.Parse(End);

            List<Appointment> TechAppts = Appointment.LoadBySchedDateRange(startDate, endDate).Where(x => x.TechnicianID == TechId).ToList();

            HttpContext.Current.Response.ContentType = "application/json";
            return ser.Serialize(TechAppts);
        }
    }
}