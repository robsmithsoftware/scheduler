﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AdamsScheduler.Models;

namespace AdamsScheduler.API
{
    [Authorize]
    public class NotesController : ApiController
    {
        // GET: api/Notes
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Notes/Date
        public Note Get(string param1)
        {
            Note note = new Note(param1);

            note.LoadByDate(note.Date);
            return note;
        }

        // POST: api/Notes
        public void Post([FromBody]Note note)
        {
            note.Save();
        }

        // PUT: api/Notes/5
        public void Put(int param1, [FromBody]string value)
        {
        }

        // DELETE: api/Notes/5
        public void Delete(int param1)
        {
        }
    }
}
