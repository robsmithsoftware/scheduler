﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TechnicianRouteForecast.aspx.cs" Inherits="AdamsScheduler.Scheduler.Report.TechnicianRouteForecast" %>
<doctype html>
<html>
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Adams Exterminators Scheduler</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.10.2.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/fullcalendar.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/TimeLine.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/routeOptimizer.js")%>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK5QuB3CXQYwkRtv9Yt-faUPt_s1y7c9I&callback=initMap" type="text/javascript"></script>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="~/css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="~/css/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print" />
    <link href="~/css/scheduler.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

</head>
<body class="noMargin">
    <h1>Technician Route Forecast</h1>
    <h2><span id="titleTech" runat="server"></span></h2>
    <h3><span id="titleDate" runat="server"></span></h3>
    
    <div class="row">
        <div class="col-sm-12">
            <div id="map" style="height: 750px;"></div>                    
        </div>
    </div>
    
    <script type="text/javascript">

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        $("#map").each(function () {
            var mapDiv = this;
            var map = new google.maps.Map(mapDiv, {
                center: { lat: 31.562310, lng: -84.187732 },
                zoom: 12
            });

            var startDate = getParameterByName("StartDate");
            var endDate = getParameterByName("EndDate");
            var id = getParameterByName("TechId");

            $.ajax({
                type: "POST",
                url: "TechnicianRouteForecast.aspx/GetAppointments",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'Start': '" + startDate + "', 'End': '" + endDate + "', 'TechId': '" + id + "'}",
                error: function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                },
                success: function (response) {
                    var data = JSON.parse(response.d);
                    var bounds = new google.maps.LatLngBounds();
                    $(data).each(function () {
                        var marker = new google.maps.Marker({
                            position: { lat: this.Latitude, lng: this.Longitude },
                            map: map,
                            title: this.Note,
                            icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + new Date(parseInt(this.AppointmentDate.substr(6))).getDate().toString() + '|4466FF|000000'
                        });
                        bounds.extend(new google.maps.LatLng(this.Latitude, this.Longitude));
                    });
                    map.fitBounds(bounds);
                    map.panToBounds(bounds);
                },
                async: false,
                cache: false
            });
        });
    </script>
</body>
</html>