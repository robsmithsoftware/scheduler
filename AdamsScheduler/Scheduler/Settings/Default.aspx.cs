﻿using AdamsScheduler.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdamsScheduler.Scheduler.Settings
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            Boolean isAdmin = manager.IsInRole(Context.User.Identity.GetUserId(), "Admin");

            TechnicianPanel.Visible = isAdmin;
        }
    }
}