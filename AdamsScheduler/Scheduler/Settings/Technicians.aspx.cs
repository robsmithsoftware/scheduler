﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using AdamsScheduler.Models;

namespace AdamsScheduler.Scheduler.Settings
{
    public partial class Technicians : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // If the user doesn't have permission, kick them out...
            if (!User.IsInRole("Admin") && !User.IsInRole("Manager")) { Response.Redirect("~/Scheduler/Settings"); }

            // If there is a technicianId in the query string then we go to detail view...
            bool isDetail = Request.QueryString["TechnicianId"] != null;
            int techId;

            // If we want to go to detail mode and can find a techid...
            if (isDetail && int.TryParse(Request.QueryString["TechnicianId"], out techId) && !IsPostBack)
            {
                Technician tech = new Technician();
                
                // If we have a tech id, load the tech else, it's a new tech so
                // hide the deactivate button.
                if (techId != 0) { tech.LoadByTechnicianID(techId); }
                else { deactivateButton.Visible = false; }

                TechIdNumber.InnerText = tech.TechID.ToString();
                TechFirstName.Value = tech.FirstName;
                TechLastName.Value = tech.LastName;

                if (!tech.Active)
                {
                    deactivateButton.InnerText = "Activate";
                }
            }
            else
            {
                // Build a table with the list of technicians.
                StringBuilder sb = new StringBuilder();
                sb.Append("<table class='table table-striped' id='listTable' runat='server'>");
                foreach (Technician tech in Technician.LoadAllTechnicians()){
                    sb.Append("<tr>");
                    sb.Append("<td><a href='Technicians?TechnicianId=" + tech.TechID + "'>" + tech.FirstName + " " + tech.LastName + " " + (!tech.Active ? "(Inactive)" : "") + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                listContent.Text = sb.ToString();
            }

            // Show the relevant panels...
            technicianDetail.Visible = isDetail;
            technicianList.Visible = !isDetail;
        }

        protected void Save(object sender, EventArgs e)
        {
            Technician tech = new Technician();

            tech.TechID = int.Parse(TechIdNumber.InnerText);
            tech.FirstName = TechFirstName.Value;
            tech.LastName = TechLastName.Value;

            tech.SaveTechnician();
            Response.Redirect("~/Scheduler/Settings/Technicians");
        }

        protected void Deactivate(object sender, EventArgs e)
        {
            Technician tech = new Technician();

            tech.LoadByTechnicianID(int.Parse(TechIdNumber.InnerText));
            tech.Active = !tech.Active;

            tech.SaveTechnician();
            Response.Redirect("~/Scheduler/Settings/Technicians");
        }
    }
}