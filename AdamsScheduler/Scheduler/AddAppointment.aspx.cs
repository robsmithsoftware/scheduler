﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AdamsScheduler.Models;

namespace AdamsScheduler.Scheduler
{
    public partial class AddAppointment : System.Web.UI.Page
    {

        private DateTime SchedDate;
        Appointment appt = new Appointment();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the current date from the query string and do stuff with it.
            AcceptDate();

            // Protect some things from postback-overwrite.
            if (!IsPostBack)
            {
                // Decide if we are looking at a new appt or editing an existing one and act accordingly.
                if (Request.QueryString["ApptID"] != null && Request.QueryString["ApptID"] != "")
                {
                    ViewState["EditMode"] = false;

                    // Load the intended appointment.
                    appt.LoadByApptID(int.Parse(Request.QueryString["ApptID"]));

                    // Check to see if we are update the appointment date.
                    if (appt.AppointmentDate != DateTime.Parse(Request.QueryString["SchedDate"].ToString().Replace('_', '/')))
                    {
                        ViewState["EditMode"] = true;
                    }
                    else {
                        appt.AppointmentDate = DateTime.Parse(Request.QueryString["SchedDate"].ToString().Replace('_', '/'));
                    }

                    // Render the appointment in the GUI.
                    DisplayAppointment(appt);
                }
                else
                {
                    ViewState["EditMode"] = true;
                }

                // Populate the technician drop down list.
                PopulateTechDdl();
                // Set the fields and buttons bases on whether the page is in edit mode.
                SetPage(bool.Parse(ViewState["EditMode"].ToString()));
            }    
        }

        #region Event Handlers
        protected void SaveAppt_Click(object sender, EventArgs e)
        {
            // Create an appointment object and set it's properties.
            try
            {
                DateTime startAsTimeSpan = DateTime.Parse(StartTime.Value);
                DateTime endAsTimeSpan = DateTime.Parse(EndTime.Value);

                appt.TechnicianID = int.Parse(TechnicianList.SelectedValue);
                // For the next two, convert the HH:mm XM format to 24-hour format, and then to a TimeSpan object.
                appt.StartTime = TimeSpan.Parse(DateTime.Parse(StartTime.Value).ToString("HH:mm")).TotalMinutes;
                appt.EndTime = TimeSpan.Parse(DateTime.Parse(EndTime.Value).ToString("HH:mm")).TotalMinutes;
                appt.AppointmentDate = SchedDate;
                appt.TravelTime = 0;
                appt.Address = Address.Value;
                appt.City = City.Value;
                appt.State = State.SelectedValue;
                appt.Zip = Zip.Value;
                appt.Note = Notes.Value;
                appt.Locked = Locked.Checked;
                
                // Use a TryParse for amount, since the amount could be empty or 0.
                Decimal AmountTemp = 0;
                decimal.TryParse(Amount.Value, out AmountTemp);
                appt.Amount = AmountTemp;

                appt.Salesman = Salesman.Value;
                appt.CustomerName = CustomerName.Value;

                if (Request.QueryString["ApptID"] != null && Request.QueryString["ApptID"] != "")
                {
                    appt.AppointmentID = int.Parse(Request.QueryString["ApptID"]);
                    saveSuccess.Visible = appt.UpdateAppointment();
                    saveFailure.Visible = !saveSuccess.Visible;
                }
                else
                {
                    appt.AppointmentID = appt.CreateNewAppointment();
                    saveSuccess.Visible = (appt.AppointmentID > 0);
                    saveFailure.Visible = !saveSuccess.Visible;
                }
            }
            catch (Exception ex) {
                saveFailure.Visible = true;
                failureMessage.InnerHtml = ex.Message;
            }

            // Set the page.
            ViewState["EditMode"] = false;
            SetPage(bool.Parse(ViewState["EditMode"].ToString()));
        }

        protected void DeleteAppt_Click(object sender, EventArgs e)
        {
            // This deletes the appointment record.
            Appointment app = new Appointment();
            app.LoadByApptID(int.Parse(Request.QueryString["ApptID"]));
            app.DeleteAppointment();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),
                    "AppointmentDeleted",
                    "alert('Appointment was deleted!');" +
                    "window.location = '" + ResolveUrl("~/Scheduler/Detail") + "?SchedDate=" + SchedDate.ToShortDateString().Replace('/', '_') + "';",
                    true);
        }

        protected void CancelAppt_Click(object sender, EventArgs e)
        {
            // This just removes edit mode.
            ViewState["EditMode"] = false;
            SetPage(bool.Parse(ViewState["EditMode"].ToString()));
        }

        protected void EditAppt_Click(object sender, EventArgs e)
        {
            // This just removes edit mode.
            ViewState["EditMode"] = true;
            SetPage(bool.Parse(ViewState["EditMode"].ToString()));
        }

        protected void ExitAppt_Click(object sender, EventArgs e)
        {
            // This redirects to the day detail page.
            Response.Redirect(ResolveUrl("~/Scheduler/Detail") + "?SchedDate=" + SchedDate.ToShortDateString().Replace('/','_'));
        }
        #endregion

        #region Helper Methods
        private void AcceptDate()
        {
            SchedDate = DateTime.MinValue;
            if (Request.QueryString["SchedDate"] != null)
            {
                DateTime.TryParse(Request.QueryString["SchedDate"].ToString().Replace('_', '-'), out SchedDate);
            }

            if (SchedDate != DateTime.MinValue)
            {
                DisplayDate.InnerHtml = SchedDate.ToLongDateString();
            }
        }
        
        private void PopulateTechDdl()
        {
            List<Technician> technicians = Technician.LoadAllTechnicians();
            List<ListItem> items = new List<ListItem>();

            foreach (Technician tech in technicians)
            {
                items.Add(new ListItem(tech.FirstName + " " + tech.LastName, tech.TechID.ToString()));
            }

            TechnicianList.DataTextField = "Text";
            TechnicianList.DataValueField = "Value";
            TechnicianList.DataSource = items;
            TechnicianList.DataBind();
        }

        private void SetPage(bool editMode)
        {
            // Set the fields...
            TechnicianList.Enabled = editMode;
            CustomerName.Disabled = !editMode;
            Salesman.Disabled = !editMode;
            StartTime.Disabled = !editMode;
            EndTime.Disabled = !editMode;
            Address.Disabled = !editMode;
            City.Disabled = !editMode;
            State.Enabled = editMode;
            Zip.Disabled = !editMode;
            Notes.Disabled = !editMode;
            Amount.Disabled = !editMode;
            Locked.Disabled = !editMode;

            // .Net likes to add a disabled class that is ugly. Override that.
            TechnicianList.CssClass = "form-control";
            State.CssClass = "form-control";

            // Set the buttons...
            SaveAppt.Visible = editMode;
            EditAppt.Visible = !editMode;
            // Show cancel, change date, and delete if we are editing a saved appointment.
            CancelAppt.Visible = editMode && Request.QueryString["ApptID"] != null;
            DeleteAppt.Visible = editMode && Request.QueryString["ApptID"] != null;
            ChangeDate.Visible = editMode && Request.QueryString["ApptID"] != null;
            // Show exit if we are not in edit mode or are editing a new appointment.
            ExitAppt.Visible = !editMode || Request.QueryString["ApptID"] == null;
        }

        private void DisplayAppointment(Appointment app)
        {
            TechnicianList.SelectedValue = app.TechnicianID.ToString();
            CustomerName.Value = app.CustomerName.ToString();
            Salesman.Value = app.Salesman.ToString();
            StartTime.Value = TimeSpan.FromMinutes(app.StartTime).ToString() + (app.StartTime > 719 ? " PM" : " AM");
            EndTime.Value = TimeSpan.FromMinutes(app.EndTime).ToString() + (app.EndTime > 719 ? " PM" : " AM");
            Address.Value = app.Address.ToString();
            City.Value = app.City.ToString();
            State.SelectedValue = app.State.ToString();
            Zip.Value = app.Zip.ToString();
            Notes.Value = app.Note.ToString();
            Amount.Value = app.Amount.ToString();
            Locked.Checked = app.Locked;
        }
        #endregion
    }
}