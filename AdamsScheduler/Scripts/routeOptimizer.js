﻿var mapObjects = [];

function initMap() {

    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;

    $("#map").each(function () {
        var mapDiv = this;
        var map = new google.maps.Map(mapDiv, {center: {lat: 31.562310, lng: -84.187732},
            zoom: 12
        });

        directionsDisplay.setMap(map);
        $("[id$='mapChoices'] option").not(":selected").each(function() {
            var waypoints = loadWaypoints($(this).val());
            calculateAndDisplayRoute(directionsService, directionsDisplay, waypoints.reverse(), false, $(this).val());
        })

        $("[id$='mapChoices'] option:selected").each(function () {
            var waypoints = loadWaypoints($(this).val());
            calculateAndDisplayRoute(directionsService, directionsDisplay, waypoints.reverse(), false, $(this).val());
        })
    });

    $("[id$='mapChoices']").change(function () {
        var techID = $("[id$='mapChoices'] option:selected").val();
        var waypoints = loadWaypoints(techID);
        calculateAndDisplayRoute(directionsService, directionsDisplay, waypoints.reverse(), false, techID);
    });

    $("[id$='optimizeDay']").click(function () {
        $("[techID]").each(function () {
            var techID = $(this).attr("techID");
            var waypoints = loadWaypoints(techID);
            calculateAndDisplayRoute(directionsService, directionsDisplay, waypoints.reverse(), true, techID);
        });
    });

    $(".optimizeRoute").click(function () {
        var techID = $(this).closest("div[techID]").attr("techID");
        var waypoints = loadWaypoints(techID);
        calculateAndDisplayRoute(directionsService, directionsDisplay, waypoints.reverse(), true, techID);
    });

    $("[id$='saveEdited']").click(function () {

        $(".appointment").each(function () {
            var target = $(this);

            var startTime = $(target).find("#startTime").text();
            var endTime = $(target).find("#endTime").text();
            var id = $(target).attr("schedid");

            $.ajax({
                type: "POST",
                url: "Detail.aspx/UpdateTimes?noCache=" + parseInt((Math.random() * 1000000).toString(), 10).toString(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'StartTime': '" + startTime + "', 'EndTime': '" + endTime + "', 'SchedID': '" + id + "'}",
                error: function (xhr, textStatus, errorThrown) {
                    alert(xhr.responseText);
                },
                success: function () {
                    var techID = $("[id$='mapChoices'] option:selected").val();
                    var waypoints = loadWaypoints(techID);
                },
                async: false,
                cache: false
            });
        });
        if (!alert("Changes Saved!")) { window.location.reload(); };
    });
    
}


function sumTravelTimes() {
    // Set the travel time for this timeline.
    $("[TechID]").each(function () {
        travelTotal = $(this).find("#travelTotal");
        $(travelTotal).html("0");

        var totMins = 0;

        $(this).find("[schedid]").each(function () {
            totMins += parseInt($(this).attr("travel"));
        });

        var hours = (Math.floor(totMins / 60) > 0) ? Math.floor(totMins / 60).toString() + ' hours ' : '';
        var minutes = ((totMins % 60) > 0) ? (totMins % 60).toString() + ' minutes' : '';
        travelTotal.html(hours + minutes);
    });

    // Set the production time for this timeline.
    $("[TechID]").each(function () {
        productionTotal = $(this).find("#productionTotal");
        $(productionTotal).html("0");

        var totMins = 0;

        $(this).find("[schedid]").each(function () {
            totMins += (parseInt($(this).attr("end")) - parseInt($(this).attr("start")));
        });

        var hours = (Math.floor(totMins / 60) > 0) ? Math.floor(totMins / 60).toString() + ' hours ' : '';
        var minutes = ((totMins % 60) > 0) ? (totMins % 60).toString() + ' minutes' : '';
        productionTotal.html(hours + minutes);
    });
}

function rearrangeWaypoints(directions, waypoints) {
    // Smush the appointments together, in order.
    var waypoint_order = directions.routes[0].waypoint_order;

    if (waypoint_order.length = waypoints.length) {

        // Get the farthest left position we can have.
        var min_left = $(waypoints[0]).parent().position().left + 1;

        // Go through each appointment and move it as far to the left as possible.
        for (var i = 0, len = waypoint_order.length; i < len; i++) {
            $(waypoints[waypoint_order[i]]).css("left", min_left);
            // Get the new minWidth, accounting for the 1px border on each side.
            min_left += $(waypoints[waypoint_order[i]]).width() + 2;
            // Update the start and end times.
            fillTime($(waypoints[waypoint_order[i]]));
        }
    }
}

function loadTravelTimes(directions, waypoints) {
    var travelTimes = [];

    for (var i = 0, len = directions.routes[0].legs.length - 1; i < len; i++) {
        travelTimes.push({
            apptId :$(waypoints[directions.routes[0].waypoint_order[i]]).attr("schedid"),
            minutes: Math.ceil(directions.routes[0].legs[i].duration.value / 60)
        });
    }

    for (var i = 0, len = travelTimes.length; i < len; i++) {
        $("[schedid=" + travelTimes[i].apptId + "]").attr("travel", travelTimes[i].minutes);
    }

    sumTravelTimes();
}

function loadWaypoints(TechID) {
    var waypoints = [];

    $("[techid='" + TechID + "']").find(".appointment").sort(function (a, b) {
        return $(a).position().left - $(b).position().left;
    }).each(function () {
        waypoints.push(this);
    });

    return waypoints.reverse();
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, waypoints, optimize, techID) {

    // Check to see if any appointments for this tech are locked. If there are then we can't do anything .....yet.
    if (optimize && $('div[techid="' + techID.toString() + '"]').find('#timeline').find('[schedid][locked="true"]').length > 0) {
        alert(
            'A timeline, which includes a locked appointment, ' +
            'cannot be automatically optimized. Please unlock ' +
            'all appointments in this timeline to optimize.'
        );
    }
    else {
        var waypts = [];

        for (var i = 0, len = waypoints.length; i < len; i++) {
            waypts.push({
                location: $(waypoints[i]).attr("address"),
                stopover: true
            });
        }

        directionsService.route({
            origin: "1702 Westtown Road, Albany GA",
            waypoints: waypts,
            destination: "1702 Westtown Road, Albany GA",
            optimizeWaypoints: optimize,
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                loadTravelTimes(response, waypoints);
                if (optimize) {
                    drawTechAppts(techID);
                    rearrangeWaypoints(response, waypoints);
                    showEditedButtons();
                }
                else {
                    drawAppts();
                }

                mapObjects = $.grep(mapObjects, function (e) { return e.techID != techID });
                mapObjects.push({ techID: techID, response: response, waypoints });

                // Only refresh the map if this calculation is for the displayed tech.
                if ($("[id$='mapChoices'] option:selected").val() == techID) {
                    directionsDisplay.setDirections(response);
                }

            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
}