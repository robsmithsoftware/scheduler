﻿using AdamsScheduler.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdamsScheduler.Scheduler.Settings
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // If the user doesn't have permission, kick them out...
            if (!User.IsInRole("Admin") && !User.IsInRole("Manager")) { Response.Redirect("~/Scheduler/Settings"); }

            // If there is a technicianId in the query string then we go to detail view...
            bool isDetail = Request.QueryString["UserId"] != null;
            string userId;

            // If we want to go to detail mode and can find a techid...
            if (isDetail && !IsPostBack)
            {
                userId = Request.QueryString["UserId"];

                Models.User user = new Models.User();
                user.LoadByUserId(userId);

                UserName.InnerText = user.UserName;
                UserId.Value = user.Id;
                enabledCheckbox.Checked = user.Enabled;

                // Check to see what roles this user has.
                ApplicationDbContext context = new ApplicationDbContext();

                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                adminCheckbox.Checked = UserManager.IsInRole(user.Id, "Admin");
                managerCheckbox.Checked = UserManager.IsInRole(user.Id, "Manager");
                technicianCheckbox.Checked = UserManager.IsInRole(user.Id, "Technician");
            }
            else
            {
                // Build a table with the list of technicians.
                StringBuilder sb = new StringBuilder();
                sb.Append("<table class='table table-striped' id='listTable' runat='server'>");
                foreach (Models.User user in Models.User.LoadAllUsers())
                {
                    sb.Append("<tr>");
                    sb.Append("<td><a href='Users?UserId=" + user.Id + "'>" + user.UserName + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                listContent.Text = sb.ToString();
            }

            // Show the relevant panels...
            userDetail.Visible = isDetail;
            userList.Visible = !isDetail;
        }

        protected void Save(object sender, EventArgs e)
        {
            Models.User user = new Models.User();

            user.Id = UserId.Value;
            user.UserName = UserName.InnerText;
            user.Enabled = enabledCheckbox.Checked;

            if (adminCheckbox.Checked) { user.Roles.Add(adminCheckbox.Value); }
            if (managerCheckbox.Checked) { user.Roles.Add(managerCheckbox.Value); }
            if (technicianCheckbox.Checked) { user.Roles.Add(technicianCheckbox.Value); }

            user.SaveRoles();
            user.Save();
        }
    }
}