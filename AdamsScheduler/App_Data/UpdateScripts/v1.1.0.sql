
CREATE TABLE [app].[Note]
(
	[Id] INT NOT NULL IDENTITY(1000,1) PRIMARY KEY,
    [Date] DATETIME NOT NULL, 
    [Text] NVARCHAR(MAX) NOT NULL, 
    [UserId] NVARCHAR(MAX) NOT NULL, 
    [AddDate] DATETIME NOT NULL,
	[EditDate] DATETIME NULL
)

﻿Alter Table app.Appointment Add Latitude Decimal(9,6)
Alter Table app.Appointment Add Longitude Decimal(9,6)

Update app.Appointment Set Latitude = 0, Longitude = 0;