﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="AdamsScheduler.Scheduler.Settings.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="userList" runat="server">
        <div class="table-responsive">
            <h2>Manage Users</h2>
            <asp:Literal id="listContent" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
    <asp:Panel ID="userDetail" runat="server">
        <h2>Edit User <span id="UserName" runat="server"></span></h2>
        <div class="row">
            <div class="col-sm-6">
                <table class="table table-striped">
                    <tr>
                        <td>Administrator</td>
                        <td><input type="checkbox" value="Admin" runat="server" id="adminCheckbox" /><br /></td>
                    </tr>
                    <tr>
                        <td>Manager</td>
                        <td><input type="checkbox" value="Manager" runat="server" id="managerCheckbox" /><br /></td>
                    </tr>
                    <tr>
                        <td>Technician</td>
                        <td><input type="checkbox" value="Technician" runat="server" id="technicianCheckbox" /></td>   
                    </tr>
                </table>     
            </div>
            <div class="col-sm-6">
                <table class="table table-striped">
                    <tr>
                        <td>Enabled</td>
                        <td><input type="checkbox" value="Enables" runat="server" id="enabledCheckbox" /><br /></td>
                    </tr>
                </table>     
            </div>
        </div>
        <div class="btn-group">
            <a class="btn btn-success" style="width:150px;" runat="server" onserverclick="Save">Save</a>
            <a class="btn btn-warning" style="width:150px;" href="Users">Cancel</a>
        </div>
        <input id="UserId" runat="server" class="hidden" />
    </asp:Panel>
</asp:Content>
