﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TechnicianDirections.aspx.cs" Inherits="AdamsScheduler.Scheduler.Report.TechnicianDirections" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Adams Exterminators Scheduler</title>

        <asp:PlaceHolder runat="server">
            <%: Scripts.Render("~/bundles/modernizr") %>
        </asp:PlaceHolder>

        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.10.2.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/fullcalendar.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/TimeLine.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/routeOptimizer.js")%>"></script>

        <webopt:bundlereference runat="server" path="~/Content/css" />
        <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <link href="~/css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="~/css/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print" />
        <link href="~/css/scheduler.css" rel="stylesheet" type="text/css"/>
        <link href="~/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="~/css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css"/>
        <link href="~/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="noMargin">
        <form id="form1" runat="server">
            <div>
                <h2 id="techName" runat="server">Technician Name</h2>
                <h3 id="schedDate" class="strong" runat="server">SchedDate</h3>
                <div class="directions"></div>
            </div>
        </form>
    </body>
    <script type="text/javascript">
        var response = $.grep(window.opener.mapObjects, function(e){
            return e.techID == getParameterByName("TechId");
        })[0].response;

        var waypoints = $.grep(window.opener.mapObjects, function (e) {
            return e.techID == getParameterByName("TechId");
        })[0].waypoints;

        directions = $('.directions');

        var routeNum = 1;
        $(response.routes[0].legs).each(function () {
            var tableText = "";
            var endAddress = this.end_address;

            tableText +=
                    "<tr>" +
                        "<td>"+
                            "<h4 class='legHeader'><span class='text-danger'>" + this.start_address + "</span>" +
                            "<br /><span class='text-success'>" + this.end_address + "</span></h4>" +
                        "</td><td><h3><strong>" + this.distance.text + "</strong></h3></td><td><h3><strong>" + this.duration.text + "</strong></h3></td>" +
                    "</tr>"

            $(this.steps).each(function () {
                tableText +=
                    "<tr>" +
                        "<td>" + this.instructions + "</td><td>" + this.distance.text + "</td><td>" + this.duration.text + "</td>" +
                    "</tr>"
                ;
            });

            $(directions).append(
                "<hr />" + 
                (routeNum <= 1 ? "<h4> Appointment " + routeNum.toString() + ", <strong>" + waypoints[response.routes[0].waypoint_order[routeNum - 1]].attributes.customer.nodeValue + "</strong></h4>" : "<h4><strong>Return</strong></h4>") +
                "<table class='table table-striped'>" + tableText + "</table>"
            );

            routeNum++;
        });
    </script>
</html>
