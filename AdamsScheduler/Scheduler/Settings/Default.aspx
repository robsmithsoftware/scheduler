﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdamsScheduler.Scheduler.Settings.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel runat="server" id="TechnicianPanel">
        <h2>Scheduler Settings</h2>
        <div class="row">
            <div class="col-sm-3">
                <a runat="server" href="~/Scheduler/Settings/Technicians">
                    <div class="well settingItem">
                        Manage Technicians<br /><br />
                        <i class="fa fa-briefcase fa-5x" aria-hidden="true"></i>
                    </div>
                 </a>
            </div>
            <div class="col-sm-3">
                <a runat="server" href="~/Scheduler/Settings/Users">
                    <div class="well settingItem">
                        Manage Users<br /><br />
                        <i class="fa fa-user fa-5x" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
