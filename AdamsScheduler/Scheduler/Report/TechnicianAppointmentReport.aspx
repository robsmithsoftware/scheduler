﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="TechnicianAppointmentReport.aspx.cs" Inherits="AdamsScheduler.Scheduler.Report.TechnicianAppointmentReport" %>
<doctype html>
<html>
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Adams Exterminators Scheduler</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.10.2.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/moment.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/fullcalendar.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/TimeLine.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/routeOptimizer.js")%>"></script>

    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="~/css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="~/css/fullcalendar.print.css" rel="stylesheet" type="text/css" media="print" />
    <link href="~/css/scheduler.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css"/>
    <link href="~/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<body class="noMargin">
    <h1>Technician Appointment Report</h1>
    <h2><span id="titleDate" runat="server"></span></h2>
    <asp:Literal id="AppointmentTable" runat="server">
        
    </asp:Literal>
    
    <script type="text/javascript">
        $('#titleDate').text(function(){
            var dates = getParameterByName("SchedDate").split("_");
            return dates.join("/");
        });

        $('.time').text(function () {
            var start = $(this).attr("startTime").toTime();
            start += start.substr(0, start.indexOf(':')) >= 7 && start.substr(0, start.indexOf(':')) != 12 ? ' AM' : ' PM';

            var end = $(this).attr("endTime").toTime();
            end += end.substr(0, end.indexOf(':')) >= 7 && end.substr(0, end.indexOf(':')) != 12 ? ' AM' : ' PM';
            return start + " - " + end;
        });
    </script>
</body>
</html>
