﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace AdamsScheduler.Models
{
    public class Appointment
    {
        private int _AppointmentID;
        private double _StartTime;
        private double _EndTime;
        private int _TechnicianID;
        private string _Color;
        private int _TravelTime;
        private DateTime _AppointmentDate;
        private string _Address;
        private string _City;
        private string _State;
        private string _Zip;
        private string _Note;
        private bool _Locked;
        private decimal _Amount;
        private string _CustomerName;
        private string _Salesman;
        private string _TechnicianName;
        private decimal _Latitude;
        private decimal _Longitude;

        public int AppointmentID { get { return _AppointmentID; } set { _AppointmentID = value; } }
        public double StartTime { get { return _StartTime; } set { _StartTime = value; } }
        public double EndTime { get { return _EndTime; } set { _EndTime = value; } }
        public int TechnicianID { get { return _TechnicianID; } set { _TechnicianID = value; } }
        public string Color { get { return _Color; } set { _Color = value; } }
        public int TravelTime { get { return _TravelTime; } set { _TravelTime = value; } }
        public DateTime AppointmentDate { get { return _AppointmentDate; } set { _AppointmentDate = value; } }
        public string Address { get { return _Address; } set { _Address = value; } }
        public string City { get { return _City; } set { _City = value; } }
        public string State { get { return _State; } set { _State = value; } }
        public string Zip { get { return _Zip; } set { _Zip = value; } }
        public string Note { get { return _Note; } set { _Note = value; } }
        public bool Locked { get { return _Locked; } set { _Locked = value; } }
        public decimal Amount { get { return _Amount; } set { _Amount = value; } }
        public string CustomerName { get { return _CustomerName; } set { _CustomerName = value; } }
        public string Salesman {  get { return _Salesman; } set { _Salesman = value; } }
        public string TechnicianName { get { return _TechnicianName; } set { _TechnicianName = value; } }
        public decimal Latitude { get { return _Latitude; } set { _Latitude = value; } }
        public decimal Longitude { get { return _Longitude; } set { _Longitude = value; } }

        private string[] Colors = new string[] { "#1abc9c","#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#e67e22", "#e74c3c", "#f39c12","#d35400","#c0392b" };
        Random rand = new Random();

        public Appointment()
        {
            this.Color = pickRandomColor(Colors);
            this.Locked = false;
        }

        public Appointment(int AppointmentID, DateTime Start, DateTime End, int TechnicianID, int TravelTime, DateTime AppointmentDate, Decimal Latitude, Decimal Longitude)
        {
            this.StartTime = TimeSpan.Parse(Start.ToString("HH:mm:ss")).TotalMinutes;
            this.EndTime = TimeSpan.Parse(End.ToString("HH:mm:ss")).TotalMinutes;
            this.AppointmentID = AppointmentID;
            this.TechnicianID = TechnicianID;
            this.Color = pickRandomColor(Colors);
            this.TravelTime = TravelTime;
            this.AppointmentDate = AppointmentDate;
            this.Locked = false;
            this.Latitude = Latitude;
            this.Longitude = Longitude;
        }

        private string pickRandomColor(string[] colors)
        {
            return colors[rand.Next(0, Colors.Length - 1)];
        }

        #region Data Access Methods

        // Load an appointment from the DB. Returns true if successful, else false.
        public Boolean LoadByApptID(int ApptID)
        {
            Boolean retVal = false;

            // Set the query that will load an Appointment by ApptID.
            string query = "SELECT AppointmentID, StartTime, EndTime, AppDate, TravelTime, TechnicianId, Address, City, State, Zip, Note, Color, Locked, Amount, CustomerName, Salesman, Latitude, Longitude FROM app.Appointment WHERE AppointmentID = @ApptID;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@ApptID", ApptID);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    if (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        this.AppointmentID = int.Parse(reader[0].ToString());
                        this.StartTime = double.Parse(reader[1].ToString());
                        this.EndTime = double.Parse(reader[2].ToString());
                        this.AppointmentDate = DateTime.Parse(reader[3].ToString());
                        this.TravelTime = int.Parse(reader[4].ToString());
                        this.TechnicianID = int.Parse(reader[5].ToString());
                        this.Address = reader[6].ToString();
                        this.City = reader[7].ToString();
                        this.State = reader[8].ToString();
                        this.Zip = reader[9].ToString();
                        this.Note = reader[10].ToString();
                        this.Color = reader[11].ToString();
                        this.Locked = (reader[12].ToString() == "True");
                        this.CustomerName = reader[14].ToString();
                        this.Salesman = reader[15].ToString();
                        this.Latitude = decimal.Parse(reader[16].ToString());
                        this.Longitude = decimal.Parse(reader[17].ToString());

                        decimal temp;
                        decimal.TryParse(reader[13].ToString(), out temp);
                        this.Amount = temp;

                        // Set retVal to true so we know the query returned a row.
                        retVal = true;
                    }
                    reader.Close();
                }
                catch
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return retVal;
        }

        // Insert an appointment into the DB. Returns this appointment's AppointmentId.
        public int CreateNewAppointment()
        {
            // Look up the latitude and longitude of this appointment.
            string address = this.Address + " " + this.City + " " + this.State + " " + this.Zip;
            string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key=AIzaSyBK5QuB3CXQYwkRtv9Yt-faUPt_s1y7c9I", Uri.EscapeDataString(address));

            WebRequest request = WebRequest.Create(requestUri);
            WebResponse response = request.GetResponse();
            XDocument xdoc = XDocument.Load(response.GetResponseStream());

            XElement result = xdoc.Element("GeocodeResponse").Element("result");
            XElement locationElement = result.Element("geometry").Element("location");
            XElement lat = locationElement.Element("lat");
            XElement lng = locationElement.Element("lng");

            this.Latitude = Decimal.Parse(lat.Value);
            this.Longitude = Decimal.Parse(lng.Value);

            // Set the query that will load a Techician by TechID.
            string query = "INSERT INTO app.Appointment(TechnicianID, StartTime, EndTime, AppDate, TravelTime, Address, City, State, Zip, Note, Color, Locked, Amount, CustomerName, Salesman, Latitude, Longitude) Values (@TechId, @StartTime, @EndTime, @AppDate, @TravelTime, @Address, @City, @State, @Zip, @Note, @Color, @Locked, @Amount, @CustomerName, @Salesman, @Latitude, @Longitude); SELECT Cast(SCOPE_IDENTITY() As int);";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@TechId", this.TechnicianID);
                command.Parameters.AddWithValue("@StartTime", this.StartTime);
                command.Parameters.AddWithValue("@EndTime", this.EndTime);
                command.Parameters.AddWithValue("@AppDate", this.AppointmentDate);
                command.Parameters.AddWithValue("@TravelTime", this.TravelTime);
                command.Parameters.AddWithValue("@Address", this.Address);
                command.Parameters.AddWithValue("@City", this.City);
                command.Parameters.AddWithValue("@State", this.State);
                command.Parameters.AddWithValue("@Zip", this.Zip);
                command.Parameters.AddWithValue("@Note", this.Note);
                command.Parameters.AddWithValue("@Color", this.Color);
                command.Parameters.AddWithValue("@Locked", this.Locked);
                command.Parameters.AddWithValue("@Amount", this.Amount);
                command.Parameters.AddWithValue("@CustomerName", this.CustomerName);
                command.Parameters.AddWithValue("@Salesman", this.Salesman);
                command.Parameters.AddWithValue("@Latitude", this.Latitude);
                command.Parameters.AddWithValue("@Longitude", this.Longitude);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    this.AppointmentID = (int)command.ExecuteScalar();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    this.AppointmentID = 0;
                }
            }

            return this.AppointmentID;
        }

        // Update an existing appointment. Returns true if successful.
        public bool UpdateAppointment()
        {
            // Set the query that will load a Techician by TechID.
            string query = "Update app.Appointment Set TechnicianID = @TechID, StartTime = @StartTime, EndTime = @EndTime, AppDate = @AppDate, TravelTime = @TravelTime, Address = @Address, City = @City, State = @State, Zip = @Zip, Note = @Note, Color = @Color, Locked = @Locked, Amount = @Amount, CustomerName = @CustomerName, Salesman = @Salesman, Latitude = @Latitude, Longitude = @Longitude Where AppointmentId = @AppointmentId;";

            // Look up the latitude and longitude of this appointment.
            string address = this.Address + " " + this.City + " " + this.State + " " + this.Zip;
            string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key=AIzaSyBK5QuB3CXQYwkRtv9Yt-faUPt_s1y7c9I", Uri.EscapeDataString(address));

            WebRequest request = WebRequest.Create(requestUri);
            WebResponse response = request.GetResponse();
            XDocument xdoc = XDocument.Load(response.GetResponseStream());

            XElement result = xdoc.Element("GeocodeResponse").Element("result");
            XElement locationElement = result.Element("geometry").Element("location");
            XElement lat = locationElement.Element("lat");
            XElement lng = locationElement.Element("lng");

            this.Latitude = Decimal.Parse(lat.Value);
            this.Longitude = Decimal.Parse(lng.Value);

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@TechId", this.TechnicianID);
                command.Parameters.AddWithValue("@StartTime", this.StartTime);
                command.Parameters.AddWithValue("@EndTime", this.EndTime);
                command.Parameters.AddWithValue("@AppDate", this.AppointmentDate);
                command.Parameters.AddWithValue("@TravelTime", this.TravelTime);
                command.Parameters.AddWithValue("@Address", this.Address);
                command.Parameters.AddWithValue("@City", this.City);
                command.Parameters.AddWithValue("@State", this.State);
                command.Parameters.AddWithValue("@Zip", this.Zip);
                command.Parameters.AddWithValue("@Note", this.Note);
                command.Parameters.AddWithValue("@Color", this.Color);
                command.Parameters.AddWithValue("@AppointmentId", this.AppointmentID);
                command.Parameters.AddWithValue("@Locked", this.Locked);
                command.Parameters.AddWithValue("@Amount", this.Amount);
                command.Parameters.AddWithValue("@CustomerName", this.CustomerName);
                command.Parameters.AddWithValue("@Salesman", this.Salesman);
                command.Parameters.AddWithValue("@Latitude", this.Latitude);
                command.Parameters.AddWithValue("@Longitude", this.Longitude);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        // Load a list of appointments for a certain date.
        public static List<Appointment> LoadBySchedDate(DateTime Date)
        {
            List<Appointment> Appointments = new List<Appointment>();

            // Set the query that will load an Appointment by date.
            string query = "SELECT AppointmentID, StartTime, EndTime, AppDate, TravelTime, TechnicianId, Address, City, State, Zip, Note, Color, Locked, Amount, CustomerName, Salesman, Latitude, Longitude FROM app.Appointment WHERE AppDate = @AppDate;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@AppDate", Date);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    while (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        Appointment app = new Appointment();

                        app.AppointmentID = int.Parse(reader[0].ToString());
                        app.StartTime = double.Parse(reader[1].ToString());
                        app.EndTime = double.Parse(reader[2].ToString());
                        app.AppointmentDate = DateTime.Parse(reader[3].ToString());
                        app.TravelTime = int.Parse(reader[4].ToString());
                        app.TechnicianID = int.Parse(reader[5].ToString());
                        app.Address = reader[6].ToString();
                        app.City = reader[7].ToString();
                        app.State = reader[8].ToString();
                        app.Zip = reader[9].ToString();
                        app.Note = reader[10].ToString();
                        app.Color = reader[11].ToString();
                        app.Locked = (reader[12].ToString() == "True");
                        app.CustomerName = reader[14].ToString();
                        app.Salesman = reader[15].ToString();
                        app.Latitude = decimal.Parse(reader[16].ToString());
                        app.Longitude = decimal.Parse(reader[17].ToString());

                        decimal temp;
                        decimal.TryParse(reader[13].ToString(), out temp);
                        app.Amount = temp;

                        // This keeps the colors different.
                        System.Threading.Thread.Sleep(1);

                        Appointments.Add(app);
                    }
                    reader.Close();
                }
                catch
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return Appointments;
        }

        // Load a list of appointments for a certain date.
        public static List<Appointment> LoadBySchedDateRange(DateTime StartDate, DateTime EndDate)
        {
            List<Appointment> Appointments = new List<Appointment>();

            // Set the query that will load an Appointment by date.
            string query = "SELECT AppointmentID, StartTime, EndTime, AppDate, TravelTime, a.TechnicianId, Address, City, State, Zip, Note, Color, Locked, Amount, CustomerName, Salesman, t.FirstName, t.LastName, a.Latitude, a.Longitude FROM app.Appointment a INNER JOIN app.Technician t on t.TechnicianId = a.TechnicianId WHERE AppDate BETWEEN @StartDate And @EndDate;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@StartDate", StartDate);
                command.Parameters.AddWithValue("@EndDate", EndDate);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    while (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        Appointment app = new Appointment();

                        app.AppointmentID = int.Parse(reader[0].ToString());
                        app.StartTime = double.Parse(reader[1].ToString());
                        app.EndTime = double.Parse(reader[2].ToString());
                        app.AppointmentDate = DateTime.Parse(reader[3].ToString());
                        app.TravelTime = int.Parse(reader[4].ToString());
                        app.TechnicianID = int.Parse(reader[5].ToString());
                        app.Address = reader[6].ToString();
                        app.City = reader[7].ToString();
                        app.State = reader[8].ToString();
                        app.Zip = reader[9].ToString();
                        app.Note = reader[10].ToString();
                        app.Color = reader[11].ToString();
                        app.Locked = (reader[12].ToString() == "True");
                        app.CustomerName = reader[14].ToString();
                        app.Salesman = reader[15].ToString();
                        app.TechnicianName = reader[16].ToString() + " " + reader[17].ToString();
                        app.Latitude = decimal.Parse(reader[18].ToString());
                        app.Longitude = decimal.Parse(reader[19].ToString());

                        decimal temp;
                        decimal.TryParse(reader[13].ToString(), out temp);
                        app.Amount = temp;

                        // This keeps the colors different.
                        System.Threading.Thread.Sleep(1);

                        Appointments.Add(app);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return Appointments;
        }

        // Load a list of appointments for a certain date.
        public static List<Appointment> LoadBySchedDateAndTech(DateTime Date, int TechId)
        {
            List<Appointment> Appointments = new List<Appointment>();

            // Set the query that will load an Appointment by date.
            string query = "SELECT AppointmentID, StartTime, EndTime, AppDate, TravelTime, TechnicianId, Address, City, State, Zip, Note, Color, Locked, Amount, CustomerName, Salesman, Latitude, Longitude FROM app.Appointment WHERE AppDate = @AppDate and TechnicianId = @TechId Order By StartTime Asc;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@AppDate", Date);
                command.Parameters.AddWithValue("@TechId", TechId);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    // Use if to get the first record.
                    while (reader.Read())
                    {
                        // Set this objects properties to the properties of the result.
                        Appointment app = new Appointment();

                        app.AppointmentID = int.Parse(reader[0].ToString());
                        app.StartTime = double.Parse(reader[1].ToString());
                        app.EndTime = double.Parse(reader[2].ToString());
                        app.AppointmentDate = DateTime.Parse(reader[3].ToString());
                        app.TravelTime = int.Parse(reader[4].ToString());
                        app.TechnicianID = int.Parse(reader[5].ToString());
                        app.Address = reader[6].ToString();
                        app.City = reader[7].ToString();
                        app.State = reader[8].ToString();
                        app.Zip = reader[9].ToString();
                        app.Note = reader[10].ToString();
                        app.Color = reader[11].ToString();
                        app.Locked = (reader[12].ToString() == "True");
                        app.CustomerName = reader[14].ToString();
                        app.Salesman = reader[15].ToString();
                        app.Latitude = decimal.Parse(reader[16].ToString());
                        app.Longitude = decimal.Parse(reader[17].ToString());

                        decimal temp;
                        decimal.TryParse(reader[13].ToString(), out temp);
                        app.Amount = temp;

                        // This keeps the colors different.
                        System.Threading.Thread.Sleep(1);

                        Appointments.Add(app);
                    }
                    reader.Close();
                }
                catch
                {
                    // TODO: Do something with the caught exception.
                }
            }
            return Appointments;
        }

        // Delete existing appointment from the DB. Returns true if successful.
        public bool DeleteAppointment()
        {
            // Set the query that will load a Techician by TechID.
            string query = "Delete From app.Appointment Where AppointmentId = @AppointmentId;";

            // Connect to the DB in the default connection string.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Create the command.
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@AppointmentId", this.AppointmentID);

                // Try to execute the command, catching any problems.
                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion
    }
}