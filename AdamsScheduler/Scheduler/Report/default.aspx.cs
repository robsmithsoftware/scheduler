﻿using AdamsScheduler.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdamsScheduler.Scheduler.Report
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserManager<ApplicationUser> manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            Boolean isManager = manager.IsInRole(Context.User.Identity.GetUserId(), "Manager");

            PopulateTechDdl();
        }

        private void PopulateTechDdl()
        {
            List<Technician> technicians = Technician.LoadAllTechnicians();
            List<ListItem> items = new List<ListItem>();

            foreach (Technician tech in technicians)
            {
                items.Add(new ListItem(tech.FirstName + " " + tech.LastName, tech.TechID.ToString()));
            }

            TechnicianList.DataTextField = "Text";
            TechnicianList.DataValueField = "Value";
            TechnicianList.DataSource = items;
            TechnicianList.DataBind();
        }
    }
}